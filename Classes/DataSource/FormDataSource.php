<?php declare(strict_types=1);

namespace Jl\HubSpot\DataSource;

use Jl\HubSpot\Domain\Marketing\FormService;
use Neos\ContentRepository\Domain\Model\NodeInterface;
use Neos\Neos\Service\DataSource\AbstractDataSource;
use Neos\Flow\Annotations as Flow;

class FormDataSource extends AbstractDataSource
{
    /**
     * @var string
     */
    protected static string $identifier = 'hubspot-form-data-source';

    /**
     * @var FormService
     * @Flow\Inject
     */
    protected $formService;

    public function getData(NodeInterface $node = null, array $arguments = [])
    {
        $options = [];

        foreach ($this->formService->getAllForms() as $form) {
            $options[$form->getId()] = ['label' => $form->getName()];
        }

        return $options;
    }
}
