<?php declare(strict_types=1);

namespace Jl\HubSpot\Domain\Marketing;

/**
 * Class Form
 * @package Jl\HubSpot\Domain\Marketing
 */
class Form
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * Form constructor.
     * @param string $id
     * @param string $name
     */
    public function __construct(string $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @param array $formResponse
     * @return static
     */
    public static function fromApi(array $formResponse)
    {
        return new static(
            $formResponse['id'],
            $formResponse['name']
        );
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
